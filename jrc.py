#!/usr/bin/env python

import os
from selenium.common.exceptions import NoSuchElementException
from urwid import *
from selenium.webdriver import *
from subprocess import Popen, PIPE
import atexit


class Menu():
    def __init__(self, title):
        self.container = [Text(title, align='center'), Divider()]
        self.start_stop_jira = self.create_item('Start Jira (Connect Ready)', None)
        self.go_to_jira = self.create_item('Login to Jira', None)
        self.install_salesforce = self.create_item('Install JIRA + Salesforce Cloud Connector (aka SFORCEJS)', None)
        self.configure_salesforce = self.create_item('Configure JIRA + Salesforce Cloud Connector', None)
        self.uninstall_salesforce = self.create_item('Uninstall JIRA + Salesforce Cloud Connector', None)
        self.soft_exit = self.create_item('Exit :: Leave Browser Open', None)
        self.exit = self.create_item('Exit :: Close Browser', None)

    def set_actions(self, actions):
        Menu.set_action(self.start_stop_jira, actions[0])
        Menu.set_action(self.go_to_jira, actions[1])
        Menu.set_action(self.install_salesforce, actions[2])
        Menu.set_action(self.configure_salesforce, actions[3])
        Menu.set_action(self.uninstall_salesforce, actions[4])
        Menu.set_action(self.soft_exit, actions[5])
        Menu.set_action(self.exit, actions[6])

    def create_item(self, title, action):
        menu_item = Button(title)
        Menu.set_action(menu_item, action)
        self.container.append(AttrMap(menu_item, 'menu_item', focus_map='menu_item_focused'))
        return menu_item

    @staticmethod
    def set_action(item, action):
        if action is not None:
            connect_signal(item, 'click', action)


class JiraStateMachine():
    def __init__(self, menu, main_loop, console):
        self.menu = menu
        self.main_loop = main_loop
        self.jira_process = None
        self.starting = False
        self.running = False
        self.stopping = False
        self.console = console

    def start_or_stop_jira(self, *_):
        if self.stopping:
            self.stopping_state(insisted=True)
        elif self.starting | self.running:
            self.stop_jira()
        else:
            self.start_jira()

    def start_jira(self, *_):
        self.starting_state()
        atlas_run_jira_connect = "atlas-run-standalone --product jira --bundled-plugins com.atlassian.plugins:atlassian-connect-plugin:1.1.0-rc.4,com.atlassian.jwt:jwt-plugin:1.1.0,com.atlassian.bundles:json-schema-validator-atlassian-bundle:1.0.4,com.atlassian.upm:atlassian-universal-plugin-manager-plugin:2.17.2,com.atlassian.webhooks:atlassian-webhooks-plugin:1.0.6 --jvmargs -Datlassian.upm.on.demand=true"
        self.jira_process = Popen(atlas_run_jira_connect, stdout=PIPE, stdin=PIPE, shell=True, preexec_fn=os.setsid)
        self.main_loop.watch_file(self.jira_process.stdout, self.jira_console_callback)

    def stop_jira(self, *_):
        self.stopping_state()
        try:
            if self.running:
                self.jira_process.communicate('\4')
            elif self.starting:
                self.jira_process.kill()
        finally:
            self.stopped_state()

    def jira_console_callback(self, *_):
        jira_output = self.jira_process.stdout.readline()
        self.console.set_text(jira_output)
        if jira_output.startswith("[INFO] Type Ctrl-C to exit"):
            self.running_state()

    def starting_state(self):
        self.starting = True
        self.menu.start_stop_jira.set_label('Jira Starting ...... Press [Enter] to cancel.')
        self.menu.exit.set_label('Exit :: Close Browser')

    def running_state(self):
        self.running = True
        self.menu.start_stop_jira.set_label('Stop Jira')
        self.menu.exit.set_label('Exit :: Stop Jira & Close Browser')

    def stopping_state(self, insisted=False):
        self.stopping = True
        if insisted:
            self.menu.start_stop_jira.set_label('Stopping Jira ...... Please be patient while we are trying to do so!')
        else:
            self.menu.start_stop_jira.set_label('Stopping Jira ......')
        self.menu.exit.set_label('Exit :: Close Browser')

    def stopped_state(self):
        self.starting = False
        self.running = False
        self.stopping = False
        self.menu.exit.set_label('Exit :: Close Browser')
        self.menu.start_stop_jira.set_label('Start Jira (Connect Ready)')


class JiraRemote:
    def __init__(self, driver):
        self.wd = None
        self.driver = driver

    def connect(self):
        self.wd = self.driver()

    # noinspection PyBroadException
    def as_admin(self, action):
        try:
            if self.wd is None:
                self.connect()
            self.login_if_needed()
            action()
        except:
            self.connect()
            self.login_if_needed()
            action()

    def is_logged_in_as_admin(self):
        wd = self.wd
        try:
            wd.implicitly_wait(.5)
            wd.find_element_by_id("system-admin-menu")
            return True
        except NoSuchElementException:
            return False

    def login_if_needed(self):
        wd = self.wd
        if not self.is_logged_in_as_admin():
            try:
                wd.implicitly_wait(1)
                wd.find_element_by_id("login-form-username").click()
            except NoSuchElementException:
                wd.get("http://localhost:2990/jira/login.jsp")
                wd.find_element_by_id("login-form-username").click()
            wd.find_element_by_id("login-form-username").clear()
            wd.find_element_by_id("login-form-username").send_keys("admin")
            wd.find_element_by_id("login-form-password").click()
            wd.find_element_by_id("login-form-password").clear()
            wd.find_element_by_id("login-form-password").send_keys("admin")
            wd.find_element_by_id("login-form-submit").click()

    def install_salesforce_connect_addon(self):
        wd = self.wd
        wd.get("http://localhost:2990/jira/plugins/servlet/upm#manage")
        wd.implicitly_wait(10)
        wd.find_element_by_id("upm-upload").click()
        wd.find_element_by_id("upm-upload-url").click()
        wd.find_element_by_id("upm-upload-url").clear()
        wd.find_element_by_id("upm-upload-url").send_keys("http://localhost:9000/atlassian-connect.json")
        wd.find_element_by_css_selector("button.button-panel-button.upm-upload-plugin-submit").click()
        wd.find_element_by_xpath("id('upm-plugin-status-dialog')//a[text()='Close']").click()

    def configure_salesforce_connect_addon(self):
        wd = self.wd
        wd.get("http://localhost:2990/jira/plugins/servlet/upm#manage")
        wd.implicitly_wait(10)
        try:
            salesforce_addon = wd.find_element_by_xpath("id('upm-manage-user-installed-plugins')//h4[contains(., 'Cloud Connector')]")
            salesforce_addon.click()
            salesforce_addon.find_element_by_xpath("//a[@title='Configure']").click()
            wd.find_element_by_xpath("id('upm-confirm-dialog')//button[text()='Uninstall add-on']").click()
        except NoSuchElementException:
            pass

    def uninstall_salesforce_connect_addon(self):
        wd = self.wd
        wd.get("http://localhost:2990/jira/plugins/servlet/upm#manage")
        wd.implicitly_wait(10)
        try:
            salesforce_addon = wd.find_element_by_xpath("id('upm-manage-user-installed-plugins')//h4[contains(., 'Cloud Connector')]")
            salesforce_addon.click()
            salesforce_addon.find_element_by_xpath("//a[@title='Uninstall']").click()
            wd.find_element_by_xpath("id('upm-confirm-dialog')//button[text()='Uninstall add-on']").click()
            wd.find_element_by_xpath("id('upm-confirm-dialog')//button[text()='Uninstall add-on']").click()
        except NoSuchElementException:
            pass

    # noinspection PyBroadException
    def quit(self):
        if self.wd is not None:
            try:
                self.wd.quit()
            except:
                pass


class JiraRemoteControl:
    """Jira Browser Remote Control"""

    def __init__(self):
        title = "JIRA + Firefox Remote Control"
        self.menu = Menu(title)
        main_widget = Padding(ListBox(SimpleFocusListWalker(self.menu.container)), left=2, right=2)
        jira_console = Text('', wrap='any', align='center')
        palette = [('footer', 'light gray', 'dark red'),
                   ('menu_item', 'yellow', 'dark blue'),
                   ('menu_item_focused', 'dark red', 'white'),
                   ('bg', 'black', 'dark blue'), ]
        self.container = Frame(main_widget, footer=AttrMap(jira_console, 'footer'))
        self.main_loop = MainLoop(self.container, palette=palette)
        self.jira = JiraStateMachine(self.menu, self.main_loop, jira_console)
        actions = [self.jira.start_or_stop_jira,
                   self.login_to_jira,
                   self.install_connect_addon,
                   self.configure_connect_addon,
                   self.uninstall_connect_addon,
                   self.soft_exit,
                   self.exit]
        self.menu.set_actions(actions)
        self.remote = JiraRemote(Firefox)

    def main(self):
        self.main_loop.run()

    def login_to_jira(self, *_):
        self.remote.as_admin(action=self.remote.login_if_needed)

    def install_connect_addon(self, *_):
        self.remote.as_admin(action=self.remote.install_salesforce_connect_addon)

    def configure_connect_addon(self, *_):
        self.remote.as_admin(action=self.remote.configure_salesforce_connect_addon)

    def uninstall_connect_addon(self, *_):
        self.remote.as_admin(action=self.remote.uninstall_salesforce_connect_addon)

    def soft_exit(self, *_):
        self.jira.stop_jira()
        raise ExitMainLoop()

    def exit(self, *_):
        self.remote.quit()
        self.soft_exit()


if '__main__' == __name__:
    jira_remote_control = JiraRemoteControl()
    atexit.register(jira_remote_control.jira.stop_jira)
    jira_remote_control.main()

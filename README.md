JIRA + Firefox Remote Control
=============================

Dependencies
------------
This project depends on the following items:

1. Python 2.7
1. urwid
1. selenium
1. subprocess

Usage
-----
Run the `jrc.py` file either with 

    $ python jrc.py 
    
or making it executable and running it with 
    
    $ ./jrc.py
    
All set, choose options and press enter or click each with mouse.    